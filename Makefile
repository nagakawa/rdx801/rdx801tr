TEX=xelatex
TEXFLAGS=-interaction=nonstopmode -halt-on-error
LATEXMK=latexmk
LATEXMKFLAGS=\
	-xelatex \
	-output-directory=out \
	-quiet \
	-interaction=nonstopmode \
	-latexoption=-halt-on-error

all: out/1.pdf out/2.pdf out/3.pdf out/4.pdf

#out/1.pdf: 1/dict/dict.tex

%/dict/dict.tex: \
		%/dict/main.dict \
		%/dict/options.json \
		workfiles/dict-to-tex.pl6
	perl6 workfiles/dict-to-tex.pl6 \
		$< \
		$@ \
		$*/dict/options.json

out/%.pdf: %/main.tex common/uruwi.sty
	mkdir -p out
	$(LATEXMK) $(LATEXMKFLAGS) -jobname=$* $< || (rm $@; false)

clean:
	rm -rf out
