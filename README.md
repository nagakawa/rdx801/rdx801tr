## x801tr

> The purpose of this document is to describe a programming problem in Experiment801 (x801) and a suggested solution, and to seek feedback on it.

### List of files

| #  | Title        | Summary                                         |
|----|--------------|-------------------------------------------------|
| 01 | Combat       | Proposal on how to implement the combat system. |
| 02 | Redux        | Analysis of requirements for x801.              |
| 03 | Requirements | More analysis of requirements.                  |
| 04 | Architecture | ???                                             |
